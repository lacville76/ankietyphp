-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Lip 2014, 09:27
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ankieta`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane`
--

CREATE TABLE IF NOT EXISTS `dane` (
  `id_ankiety` int(11) NOT NULL AUTO_INCREMENT,
  `pers1` int(11) NOT NULL,
  `pers2` int(11) NOT NULL,
  `pers3` int(11) NOT NULL,
  `pers4` int(11) NOT NULL,
  `pers5` int(11) NOT NULL,
  `pers6` int(11) NOT NULL,
  `pers7` int(11) NOT NULL,
  `pers8` int(11) NOT NULL,
  `prof1` int(11) NOT NULL,
  `prof2` int(11) NOT NULL,
  `prof3` int(11) NOT NULL,
  `prof4` int(11) NOT NULL,
  `prof5` int(11) NOT NULL,
  `prof6` int(11) NOT NULL,
  `prof7` int(11) NOT NULL,
  `prof8` int(11) NOT NULL,
  `prof9` int(11) NOT NULL,
  `prof10` int(11) NOT NULL,
  `prof11` int(11) NOT NULL,
  `prof12` int(11) NOT NULL,
  `prof13` int(11) NOT NULL,
  `prof14` int(11) NOT NULL,
  `prof15` int(11) NOT NULL,
  `prof16` int(11) NOT NULL,
  `prof17` int(11) NOT NULL,
  `prof18` int(11) NOT NULL,
  `prof19` int(11) NOT NULL,
  `imie` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `mail` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `projekt` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_od` date DEFAULT NULL,
  `data_do` date DEFAULT NULL,
  `panstwo` varchar(40) COLLATE utf8_polish_ci DEFAULT NULL,
  `miasto` varchar(40) COLLATE utf8_polish_ci DEFAULT NULL,
  `charak_proj` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `kompetencje_osobowe` varchar(8) COLLATE utf8_polish_ci DEFAULT NULL,
  `przygotowanie` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `podroz` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `zakwaterowanie` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `wycieczki` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `dzialania_proj` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `youthpass` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `inne` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `kompetencje_zawodowe` varchar(19) COLLATE utf8_polish_ci DEFAULT NULL,
  `kompetencje_zawodowe_inne` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `mail_1` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `mail_2` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `mail_3` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `mail_4` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `zgoda` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_ankiety`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=39 ;

--
-- Zrzut danych tabeli `dane`
--

INSERT INTO `dane` (`id_ankiety`, `pers1`, `pers2`, `pers3`, `pers4`, `pers5`, `pers6`, `pers7`, `pers8`, `prof1`, `prof2`, `prof3`, `prof4`, `prof5`, `prof6`, `prof7`, `prof8`, `prof9`, `prof10`, `prof11`, `prof12`, `prof13`, `prof14`, `prof15`, `prof16`, `prof17`, `prof18`, `prof19`, `imie`, `nazwisko`, `mail`, `projekt`, `data_od`, `data_do`, `panstwo`, `miasto`, `charak_proj`, `kompetencje_osobowe`, `przygotowanie`, `podroz`, `zakwaterowanie`, `wycieczki`, `dzialania_proj`, `youthpass`, `inne`, `kompetencje_zawodowe`, `kompetencje_zawodowe_inne`, `mail_1`, `mail_2`, `mail_3`, `mail_4`, `zgoda`) VALUES
(37, 6, 5, 4, 3, 4, 4, 2, 4, 4, 2, 3, 3, 5, 3, 5, 2, 6, 2, 6, 2, 4, 2, 5, 3, 2, 4, 3, '', '', '', '', '2005-01-01', '2005-01-01', '', 'Abchazja', '', NULL, '', '', '', '', '', '', '', '', NULL, '', '', '', '', 0),
(38, 3, 2, 3, 1, 6, 2, 5, 3, 6, 4, 2, 5, 3, 6, 2, 4, 2, 4, 2, 3, 3, 3, 2, 5, 3, 1, 6, '', '', '', '', '2005-01-01', '2005-01-01', '', 'Abchazja', '', NULL, '', '', '', '', '', '', '', '', NULL, '', '', '', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
