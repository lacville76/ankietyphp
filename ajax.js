
$.ajax({
    type: "POST",
    url: "/insert.php",
    dataType: 'json',
});

function getCook(cookiename)
{
    // Get name followed by anything except a semicolon
    var cookiestring = RegExp("" + cookiename + "[^;]+").exec(document.cookie);
    // Return everything after the equal sign
    return unescape(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : "");

}

//


$(document).ready(function() {

    $('#form1').ajaxForm(function(response) {
        var cookieValue = getCook('message');
        var javaScriptVar = cookieValue;
         
       if (javaScriptVar == 2) {
            new Messi('Prosimy o zaznaczenie wszystkich opcji', {title: 'Powiadomienie', modal: true});
        }
        if (javaScriptVar == 4) {
            new Messi('Prosimy o wypelnienie wymaganych pól', {title: 'Powiadomienie', modal: true});
        }
        if (javaScriptVar == 3) {
            new Messi('Dziekujemy za wypelnienie ankiety', {title: 'Powiadomienie', modal: true});
        }
        if (javaScriptVar == 5) {
            new Messi('Na podany adres e-mail została już wysłana prośba o weryfikację', {title: 'Powiadomienie', modal: true});
        }
        if (javaScriptVar == 1) {
            new Messi('Wymagana jest zgoda', {title: 'Powiadomienie', modal: true});
        }
        
    });
});


function printAnk()
{
    $("#LangImg").hide();
    $("#SubmitBtn").hide();
    $("#PrintBtn").hide();

    window.print();
    $("#LangImg").show();
    $("#SubmitBtn").show();
    $("#PrintBtn").show();
}




/*  formularz */

$( function(){
    var i = 0;
    
    /* checkbox */
    $(".checboxy input:checkbox").on('click', function() {
        if( $(this).parent().parent().find("input:checked").length <= 3 ) {
            if( $(this).prop("checked") ) {
                $(this).prop("checked", true);
            }
        }
        else {
            $(this).prop("checked", false);
            $(this).parent().parent().find("input:checked").slice(3).prop('checked',false);
            BootstrapDialog.show({
                type    : BootstrapDialog.TYPE_DANGER,
                title   : 'Błąd na stronie - Typy projektów',
                message : 'Maksymalnie można zaznaczyć 3 typy projektów',
                buttons : [{
                    label   : 'Zamknij okno',
                    action  : function(box){
                        box.close();
                    }
                }]
            });
        }
           
    });    
    
    /* select */
    $('.selects select').selectpicker();
    
    /* sliders */
    $('.sliders input').css('width','300px');
    $('.sliders input').slider({min:0, max:6, value:3});
    
    
    
});