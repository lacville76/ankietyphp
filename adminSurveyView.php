<!doctype html>
<html lang="pl">

<head>
	<meta charset="utf-8"/>
	<title></title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});


       
//          ['Umiejętności językowe (angielski, niemiecki, francuski)', <?php echo $s9; ?>],

      
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
    
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="indexAdmin.html">Panel administracyjny</a></h1>
			
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Administrator</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
		<div class="languageButton"><img src="images/flags/Angielski.png" alt="some_text" onclick="location.href='adminSurveyViewEng.php'"></div>
		
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<hr/>
		<h3>Statystyki</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminPersonal.php">Kompetencjie personalne</a></li>
                        <li class="icn_categories"><a href="adminProffesional.php">Kompetencjie zawodowe</a></li>
			<li class="icn_categories"><a href="adminVoluntary.php">Wolontariusze</a></li>

		</ul>
		<h3>Ankiety</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminSurveyView.php">Podgląd ankiet</a></li>
		</ul>

		
		<footer>
			
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
            <div style="margin-left: 10px;">
            <h2>Lista wypełnionych ankiet</h2>
            <?php include('ViewList.php') ?>
            </div>
		
		
	</section>


</body>

</html>

