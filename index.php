<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="js/jquery-2.1.1.min.js"></script> 
        <script src="js/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/bootstrap/dialog/js/bootstrap-dialog.min.js"></script>
        <script src="js/bootstrap/select/bootstrap-select.min.js"></script>
        <script src="js/bootstrap/slider/js/bootstrap-slider.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="js/bootstrap/dialog/css/bootstrap-dialog.min.css" />
        <link rel="stylesheet" href="js/bootstrap/select/bootstrap-select.min.css" />
        <link rel="stylesheet" href="js/bootstrap/slider/css/slider.css" />
        <link rel="stylesheet" href="css/messi.min.css" />
        <link rel="stylesheet" href="style.css" />


        <script src="js/messi.js"></script>
        <script src="ajax.js" type="text/javascript"></script>

        <title>Wolontariat europejski - Formularz</title>
    </head>

    <body>
        <div class="languageButton">
            <a href="indexEng.php">
                <img src="images/flags/Angielski.png" alt="Angielski" id="LangImg" title="Zmiana języka na Angielski / Change language to English">
            </a>
        </div>

        <div class="containers">
            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">EWALUACJA KOŃCOWA UDZIAŁU W PROGRAMIE</div>
                    <div class="panel-body">
                        <h3 class="text-center" >Wolontariat europejski</h3>
                        <h4 class="text-center" >Jeżeli nie ma pan/pani nic do wpisania w dane pole proszę zostawić bez zmian</h4>                    

                        <br />

                        <form Name ="form1" Method ="post" id="form1" ACTION = "insert.php">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Imię:</label>
                                    <input type="text"   name="name" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Nazwisko:</label> 
                                    <input type="text" name="surname" class="form-control" />
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>E-Mail: </label>
                                    <input type="text" name="mail" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Tytul projektu:</label>  
                                    <input type="text" name="project" class="form-control" />
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-12"><label>Typy projektów:</label></div>
                                <div class="col-md-12 checboxy checkbox">

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio" name="fooby1[]" id="fooby1"  />
                                        Społeczno-integracyjne
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby2[]" id="fooby2" />
                                        Pomocy osobom niepełnosprawnym
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby3[]" id="fooby3" />
                                        Współpracy z młodzieżą
                                    </label>

                                    <label class="col-md-4">
                                        <input  type="checkbox" class="radio"  name="fooby4[]"  id="fooby4"/>
                                        Współpracy z dziećmi
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby5[]" id="fooby5" />
                                        Kulturalno-artystyczne
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby6[]" id="fooby6" />
                                        Medialne i promocyjne
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby7[]" id="fooby7"/>
                                        Przedsiębiorcze, rozwoju konkurencji
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby8[]" id="fooby8"/>
                                        Środowiskowe i ekologiczne
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby9[]" id="fooby9"/>
                                        Związane z tolerancją
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio" name="fooby10[]" id="fooby10"/>
                                        Turystyczno-rekreacyjne
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby11[]" id="fooby11"/>
                                        Sportowo-rekreacyjne lub taneczne
                                    </label>                        
                                </div>
                            </div> 

                            <div class="form-group row selects">
                                <div class="col-md-6">
                                    <label>Okres pobytu od:</label> <br />
                                    <div class="col-md-3">
                                        <select name="fday" class="form-control" >
                                            <option value='01'>01</option>
                                            <option value='02'>02</option>
                                            <option value='03'>03</option>
                                            <option value='04'>04</option>
                                            <option value='05'>05</option>
                                            <option value='06'>06</option>
                                            <option value='07'>07</option>
                                            <option value='08'>08</option>
                                            <option value='09'>09</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select name="fmonth" class="form-control" >
                                            <option value='01'>Styczeń</option>
                                            <option value='02'>Luty</option>
                                            <option value='03'>Marzec</option>
                                            <option value='04'>Kwiecień</Kwiecie></option>
                                            <option value='05'>Maj</option>
                                            <option value='06'>Czerwiec</option>
                                            <option value='07'>Lipiec</option>
                                            <option value='08'>Sierpień</option>
                                            <option value='09'>Wrzesie</option>
                                            <option value='10'>Październik</option>
                                            <option value='11'>Listopad</option>
                                            <option value='12'>Grudzień</option>
                                        </select>  
                                    </div>
                                    <div class="col-md-4">
                                        <select name="fyear" class="form-control" >
                                            <option value='2005'>2005</option>
                                            <option value='2006'>2006</option>
                                            <option value='2007'>2007</option>
                                            <option value='2008'>2008</option>
                                            <option value='2009'>2009</option>
                                            <option value='2010'>2010</option>
                                            <option value='2011'>2011</option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                            <option value='2015'>2015</option>
                                            <option value='2016'>2016</option>
                                        </select>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Okres pobytu do:</label> <br />
                                    <div class="col-md-3">
                                        <select name="tday" class="form-control" >
                                            <option value='01'>01</option>
                                            <option value='02'>02</option>
                                            <option value='03'>03</option>
                                            <option value='04'>04</option>
                                            <option value='05'>05</option>
                                            <option value='06'>06</option>
                                            <option value='07'>07</option>
                                            <option value='08'>08</option>
                                            <option value='09'>09</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select name="tmonth" class="form-control" >
                                            <option value='01'>Styczeń</option>
                                            <option value='02'>Luty</option>
                                            <option value='03'>Marzec</option>
                                            <option value='04'>Kwiecień</Kwiecie></option>
                                            <option value='05'>Maj</option>
                                            <option value='06'>Czerwiec</option>
                                            <option value='07'>Lipiec</option>
                                            <option value='08'>Sierpień</option>
                                            <option value='09'>Wrzesie</option>
                                            <option value='10'>Październik</option>
                                            <option value='11'>Listopad</option>
                                            <option value='12'>Grudzień</option>
                                        </select>  
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tyear" class="form-control" >
                                            <option value='2005'>2005</option>
                                            <option value='2006'>2006</option>
                                            <option value='2007'>2007</option>
                                            <option value='2008'>2008</option>
                                            <option value='2009'>2009</option>
                                            <option value='2010'>2010</option>
                                            <option value='2011'>2011</option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                            <option value='2015'>2015</option>
                                            <option value='2016'>2016</option>
                                        </select>    
                                    </div>                                    
                                </div>
                            </div> 

                            <div class="form-group selects row">
                                <div class="col-md-6">
                                    <label>Panstwo:</label>
                                    <select name="country" class="form-control">
                                        <?php
                                        $nationalityList = array("Afganistan", "Albania", "Algieria", "Andora", "Angola", "Anguilla", "Antarktyda",
                                            "Antigua i Barbuda", "Arabia Saudyjska", "Argentyna", "Armenia", "Aruba", "Australia",
                                            "Austria", "Azerbejdżan", "Bahamy", "Bahrajn", "Bangladesz", "Barbados", "Belgia", "Belize",
                                            "Benin", "Bermudy", "Bhutan", "Białoruś", "Boliwia", "Bonaire, Sint Eustatius i Saba",
                                            "Bośnia i Hercegowina", "Botswana", "Brazylia", "Brunei Darussalam", "Bułgaria",
                                            "Burkina Faso", "Burundi", "Ceuta", "Chile", "Chiny", "Curaçao", "Chorwacja", "Cypr",
                                            "Czad", "Czarnogóra", "Dania", "Dominika", "Dominikana", "Dżibuti", "Egipt", "Ekwador",
                                            "Erytrea", "Estonia", "Etiopia", "Falklandy", "Fidżi Republika", "Filipiny", "Finlandia",
                                            "Francja", "Gabon", "Gambia", "Ghana", "Gibraltar", "Grecja", "Grenada", "Grenlandia",
                                            "Gruzja", "Guam", "Gujana", "Gwatemala", "Gwinea", "Gwinea Równikowa", "Gwinea-Bissau",
                                            "Haiti", "Hiszpania", "Honduras", "Hongkong", "Indie", "Indonezja", "Irak", "Iran",
                                            "Irlandia", "Islandia", "Izrael", "Jamajka", "Japonia", "Jemen", "Jordania", "Kajmany",
                                            "Kambodża", "Kamerun", "Kanada", "Katar", "Kazachstan", "Kenia", "Kirgistan", "Kiribati",
                                            "Kolumbia", "Komory", "Kongo", "Kongo, Republika Demokratyczna",
                                            "Koreańska Republika Ludowo-Demokratyczna", "Kosowo", "Kostaryka", "Kuba", "Kuwejt",
                                            "Laos", "Lesotho", "Liban", "Liberia", "Libia", "Liechtenstein", "Litwa", "Luksemburg",
                                            "Łotwa", "Macedonia", "Madagaskar", "Majotta", "Makau", "Malawi", "Malediwy", "Malezja",
                                            "Mali", "Malta", "Mariany Północne", "Maroko", "Mauretania", "Mauritius", "Meksyk",
                                            "Melilla", "Mikronezja", "Mołdowa", "Mongolia", "Montserrat", "Mozambik", "Myanmar (Burma)",
                                            "Namibia", "Nauru", "Nepal", "Niderlandy", "Niemcy", "Niger", "Nigeria", "Nikaragua",
                                            "Niue", "Norfolk", "Norwegia", "Nowa Kaledonia",
                                            "Nowa Zelandia", "Okupowane Terytorium Palestyny", "Oman", "Pakistan", "Palau", "Panama",
                                            "Papua Nowa Gwinea", "Paragwaj", "Peru", "Pitcairn", "Polinezja Francuska", "Polska",
                                            "Południowa Georgia i Południowe Wyspy Sandwich", "Portugalia", "Republika Czeska",
                                            "Republika Korei", "Rep.Połud.Afryki", "Rep.Środkowoafryańska", "Rosja", "Rwanda",
                                            "Sahara Zachodnia", "Saint Barthelemy", "Rumunia", "Salwador", "Samoa", "Samoa Amerykańskie",
                                            "San Marino", "Senegal", "Serbia", "Seszele", "Sierra Leone", "Singapur", "Suazi", "Słowacja",
                                            "Słowenia", "Somalia", "Sri Lanka", "St. Pierre i Miquelon", "St.Kitts i Nevis", "St.Lucia",
                                            "St.Vincent i Grenadyny", "Stany Zjedn. Ameryki", "Sudan", "Sudan Południowy", "Surinam",
                                            "Syria", "Szwajcaria", "Szwecja", "Święta Helena", "Tadżykistan", "Tajlandia", "Tajwan",
                                            "Tanzania", "Togo", "Tokelau", "Tonga", "Trynidad i Tobago", "Tunezja", "Turcja", "Turkmenistan",
                                            "Wyspy Turks i Caicos", "Tuvalu", "Uganda", "Ukraina", "Urugwaj", "Uzbekistan", "Vanuatu", "Wallis i Futuna",
                                            "Watykan", "Wenezuela", "Węgry", "Wielka Brytania", "Wietnam", "Włochy", "Wschodni Timor", "Wyb.Kości Słoniowej",
                                            "Wyspa Bouveta", "Wyspa Bożego Narodzenia", "Wyspy Cooka", "Wyspy Owcze", "Wyspy Marshalla", "Wyspy Salomona",
                                            "Wyspy Św.Tomasza i Książęca", "Zambia", "Zielony Przylądek", "Zimbabwe", "Zjedn.Emiraty Arabskie");
                                        foreach ($nationalityList as $cataloguePDFName) {
                                            echo '<option value="' . $cataloguePDFName . '">' . $cataloguePDFName . '</option>';
                                        }
                                        
                                        ?>
                                    </select>                                    
                                </div>
                                <div class="col-md-6">
                                    <label> Miasto:</label>  
                                    <input type="text" name="city" class="form-control"/>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Proszę krótko scharakteryzować projekt (na czym polegał, co było jego celem, kto był zaangażowany w realizację projektu, do kogo projekt był skierowany)</label>
                                    <textarea name="charProjektu" rows="4" cols="50" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Jakie kompetencje osobiste podniósł/a Pan/ Pani podczas Wolontariatu Europejskiego</label>
                                </div>
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    1. Umiejętność dostrzegania potrzeb innych ludzi:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp1" value="0" />
                                </div>
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    2. Wrażliwość (świadomość emocjonalna) odmiennej kultury:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp2" value="0" />
                                </div>                                
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    3. Otwartość na ludzi, nowe znajomości i przyjaźnie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp3" value="0" />
                                </div>                                 
                            </div>         

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    4. Samodzielność w czynnościach dnia codziennego:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp4" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    5. Wrażliwość na potrzeby osób niepełnosprawnych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp5" value="0">
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    6. Samodzielność w podejmowaniu decyzji:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp6" value="0" />
                                </div>                                 
                            </div>       

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    7. Pewność siebie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp7" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    8. Asertywność:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp8" value="0" />
                                </div>                                 
                            </div>    

                            <div class="form-group row">
                                <br /><br />
                                <div class="col-md-12">
                                    <label>Jakie informacje / dobre rady, przekazałaby Pani innym wolontariuszom / szkom biorącym udział w Wolontariacie Europejskim</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Przygotowanie do udziału w projekcie:</label>
                                    <textarea name="przygotowanie" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Podróż:</label>
                                    <textarea name="podroz"rows="4" cols="50" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Zakwaterowanie i wyżywienie:</label>
                                    <textarea name="zakwaterowanie" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Wycieczki i podróże:</label>
                                    <textarea name="wycieczki"  rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Działania projektu i ich organizacja:</label>
                                    <textarea name="dzialania_proj" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label >Youthpass:</label>
                                    <textarea name="youthpass"  rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Inne:</label>
                                    <textarea name="inne" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Jakie kompetencje zawodowe podniósł/a Pan/Pani podczas Wolontariatu Europejskiego</label>
                                </div>
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    1. Umiejętności językowe (angielski, niemiecki, francuski):
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp1" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    2. Umiejętności językowe kraju projektu EVS:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp2" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    3. Motywacja do uczenia się języków obcych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp3" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    4. Motywacja do zdobywania kwalifikacji międzynarodowych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp4" value="0" />
                                </div>                                 
                            </div>     

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    5. Umiejętności fotograficzne:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp5" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    6. Organizacja czasu pracy i projektu:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp6" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    7. Zdolności prezentacyjne w języku obcym:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp7" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    8. Umiejętności opieki nad osobami niepełnosprawnymi:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp8" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    9. Umiejętności opieki nad dziećmi i młodzieżą:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp9" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    10. Umiejętności promowania imprez, szkoleń, akcji:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp10" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    11. Umiejętność kreowania projektów / inicjatyw:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp11" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    12. Umiejętność pracy w zespole projektowym:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp12" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    13. Kompetencje zawodowe związane z ochroną przyrody:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp13" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    14. Kompetencje zawodowe dotyczące zdrowego stylu życia:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp14" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    15. Kompetencje nauczania w zakresie sportu, rekreacji:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp15" value="0" />
                                </div>                                 
                            </div>         

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    16. Kompetencje teatralno - aktorskie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp16" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    17. Kompetencje dziennikarskie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp17" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    18. Obsługa komputera programów i stron internetowych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp18" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row">
                                <div class="col-md-1 text-right">
                                    <label class="control-label">Inne:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" name="kompetencje_zawodowe" class="form-control">
                                </div>              
                                <div class="col-md-4 sliders">
                                    <input type="text" name="pkomp19" value="0" />
                                </div>
                            </div>       

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Kogo zaprosiłbyś / zaprosiłabyś do udziału w programie:</label>
                                </div>                               
                            </div>       

                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 1: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-1" class="form-control" />
                                </div>                                 
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 2: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-2" class="form-control" />
                                </div>                                 
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 3: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-3" class="form-control" />
                                </div>                                 

                                <div class="col-md-1">
                                    <label class="control-label">E-mail 4: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-4" class="form-control" />
                                </div>                                 
                            </div>                             

                            <div class="form-group">
                                <div class="input-group col-md-6">
                                    <div  class="input-group-addon">
                                        <input type="checkbox" name="zgoda[]"  id="zgoda" />
                                    </div>
                                    <input name="" value="Wyrażam zgodę na zamieszczenie moich opinii na portalu EVS" class="form-control disabled" disabled="disabled" />
                                </div>
                            </div>                        

                            <div class="form-group text-right">
                                <INPUT TYPE = "Submit" id="SubmitBtn" class="btn btn-primary" Name = "Submit1" VALUE = "Wyślij" onClick="Update();">
                                <INPUT TYPE = "Button" id="PrintBtn"  class="btn btn-default" Name = "Submit2" VALUE = "Drukuj" onclick="printAnk();">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="mainDiv">

            <h4></h4>

            <p style="padding-top: 20px"></p></div>
        <div class="btns">

        </div>
    </div>
</body>
</html>
