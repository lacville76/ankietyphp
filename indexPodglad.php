<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="js/jquery-2.1.1.min.js"></script> 
        <script src="js/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/bootstrap/dialog/js/bootstrap-dialog.min.js"></script>
        <script src="js/bootstrap/select/bootstrap-select.min.js"></script>
        <script src="js/bootstrap/slider/js/bootstrap-slider.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="js/bootstrap/dialog/css/bootstrap-dialog.min.css" />
        <link rel="stylesheet" href="js/bootstrap/select/bootstrap-select.min.css" />
        <link rel="stylesheet" href="js/bootstrap/slider/css/slider.css" />
        <link rel="stylesheet" href="css/messi.min.css" />
        <link rel="stylesheet" href="style.css" />


        <script src="js/messi.js"></script>
        <script src="ajax.js" type="text/javascript"></script>

        <title>Podgląd ankiety</title>
    </head>

    <body>
       

        <div class="containers">
            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">PODGLĄD ANKIETY</div>
                    <div class="panel-body">
                        <h3 class="text-center" >Wolontariat europejski</h3>
                                         

                        <br />

                        <form Name ="form1" Method ="post" id="form1" ACTION = "insert.php">
                            <?php include('phpPodglad.php') ?>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Imię:</label>
                                    <input type="text"  readonly="true"   value="<?=$imie?>" name="name" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Nazwisko:</label> 
                                    <input type="text" name="surname" value="<?=$nazwisko?>"  class="form-control"  readonly="true"/>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>E-Mail: </label>
                                    <input type="text" name="mail"  value="<?=$mail?>" class="form-control"  readonly="true"/>
                                </div>
                                <div class="col-md-6">
                                    <label>Tytul projektu:</label>  
                                    <input type="text" name="project" value="<?=$projekt?>" class="form-control" readonly="true"/>
                                </div>
                            </div> 


                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Pobyt od: </label>
                                    <input type="text" name="mail"  value="<?=$data_od?>" class="form-control" readonly="true"/>
                                </div>
                                <div class="col-md-6">
                                    <label>Pobyt do:</label>  
                                    <input type="text" name="project"  value="<?=$data_do?>" class="form-control" readonly="true"/>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Panstwo: </label>
                                    <input type="text" name="mail"  value="<?=$panstwo?>" class="form-control" readonly="true"/>
                                </div>
                             
                                <div class="col-md-6">
                                    <label> Miasto:</label>  
                                    <input type="text" value="<?=$miasto?>" class="form-control" readonly="true"/>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Proszę krótko scharakteryzować projekt (na czym polegał, co było jego celem, kto był zaangażowany w realizację projektu, do kogo projekt był skierowany)</label>
                                    <textarea name="charProjektu" rows="4" cols="50" value="<?=$charak_proj?>" class="form-control"  readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Jakie kompetencje osobiste podniósł/a Pan/ Pani podczas Wolontariatu Europejskiego</label>
                                </div>
                            </div>

                             <div class="form-control">
                                <div class="col-md-8">
                                    1. Umiejętność dostrzegania potrzeb innych ludzi:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s1?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                             <div class="form-control">
                                <div class="col-md-8">
                                    2. Wrażliwość (świadomość emocjonalna) odmiennej kultury:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s2?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                                <div class="form-control">
                                <div class="col-md-8">
                                    3. Otwartość na ludzi, nowe znajomości i przyjaźnie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s3?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>
      

                             <div class="form-control">
                                <div class="col-md-8">
                                    4. Samodzielność w czynnościach dnia codziennego:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s4?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                             <div class="form-control">
                                <div class="col-md-8">
                                    5. Wrażliwość na potrzeby osób niepełnosprawnych
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s5?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    6. Samodzielność w podejmowaniu decyzji:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s6?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                             <div class="form-control">
                                <div class="col-md-8">
                                    7. Pewność siebie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s7?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                              <div class="form-control">
                                <div class="col-md-8">
                                    8. Asertywność:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s8?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>


                            <div class="form-group row">
                                <br /><br />
                                <div class="col-md-12">
                                    <label>Jakie informacje / dobre rady, przekazałaby Pani innym wolontariuszom / szkom biorącym udział w Wolontariacie Europejskim</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Przygotowanie do udziału w projekcie:</label>
                                    <textarea readonly="true" name="przygotowanie" rows="4" cols="50" class="form-control" ><?=$przygotowanie?></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Podróż:</label>
                                    <textarea readonly="true" name="podroz"rows="4" cols="50" class="form-control"><?=$podroz?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Zakwaterowanie i wyżywienie:</label>
                                    <textarea  readonly="true" name="zakwaterowanie" rows="4" cols="50" class="form-control"><?=$zakwaterowanie?></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Wycieczki i podróże:</label>
                                    <textarea  readonly="true" name="wycieczki"  rows="4" cols="50" class="form-control"><?=$wycieczki?></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Działania projektu i ich organizacja:</label>
                                    <textarea readonly="true" name="dzialania_proj" rows="4" cols="50" class="form-control"><?=$dzialania_proj?></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label >Youthpass:</label>
                                    <textarea readonly="true" name="youthpass"  rows="4" cols="50" class="form-control"><?=$youthpass?></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Inne:</label>
                                    <textarea readonly="true" name="inne" rows="4" cols="50" class="form-control"><?=$inne?></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Jakie kompetencje zawodowe podniósł/a Pan/Pani podczas Wolontariatu Europejskiego</label>
                                </div>
                            </div>   

                           <div class="form-control">
                                <div class="col-md-8">
                                    1. Umiejętności językowe (angielski, niemiecki, francuski):
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s9?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>
                            
                             <div class="form-control">
                                <div class="col-md-8">
                                    2. Umiejętności językowe kraju projektu EVS:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s10?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    3. Motywacja do uczenia się języków obcych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s11?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                           <div class="form-control">
                                <div class="col-md-8">
                                    4. Motywacja do zdobywania kwalifikacji międzynarodowych:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s12?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>
                            
                             <div class="form-control">
                                <div class="col-md-8">
                                    5. Umiejętności fotograficzne:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s13?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                             <div class="form-control">
                                <div class="col-md-8">
                                    6. Organizacja czasu pracy i projektu:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s14?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    7. Zdolności prezentacyjne w języku obcym:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s15?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                          <div class="form-control">
                                <div class="col-md-8">
                                    8. Umiejętności opieki nad osobami niepełnosprawnymi:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s16?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    9. Umiejętności opieki nad dziećmi i młodzieżą
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s17?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                             <div class="form-control">
                                <div class="col-md-8">
                                    10. Umiejętności promowania imprez, szkoleń, akcji
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s18?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                             <div class="form-control">
                                <div class="col-md-8">
                                    11. Umiejętność kreowania projektów / inicjatyw:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s19?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    12.Umiejętność pracy w zespole projektowym:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s20?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                             <div class="form-control">
                                <div class="col-md-8">
                                    13.  Kompetencje zawodowe związane z ochroną przyrody:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s21?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                              <div class="form-control">
                                <div class="col-md-8">
                                    14. Kompetencje zawodowe dotyczące zdrowego stylu życia: 
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s22?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="col-md-8">
                                    15.  Kompetencje nauczania w zakresie sportu, rekreacji: 
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s23?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>     

                            <div class="form-control">
                                <div class="col-md-8">
                                    16. Kompetencje teatralno - aktorskie:
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s24?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                          <div class="form-control">
                                <div class="col-md-8">
                                    17. Kompetencje dziennikarskie: 
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s25?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                           <div class="form-control">
                                <div class="col-md-8">
                                    18. Obsługa komputera programów i stron internetowych: 
                                </div>
                                <div class="col-md-4">
                                    <input type="text"   value="<?=$s26?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>

                        <div class="form-control">
                                <div class="col-md-1 text-right">
                                    <label class="control-label">Inne:</label>
                                </div>
                                <div class="col-md-7">
                                   <input type="text" name="kompetencje_zawodowe" value="<?=$kompetencje_zawodowe?>" readonly="true">
                                </div>              
                                  <div class="col-md-4">
                                  <input type="text"  value="<?=$s27?>" name="project" align="left" readonly="true" size="2"/>
                                </div>
                            </div>        
                           

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Kogo zaprosiłbyś / zaprosiłabyś do udziału w programie:</label>
                                </div>                               
                            </div>       
                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 1: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-1" class="form-control" readonly="true"  value="<?=$mail_1?>"/>
                                </div>                                 
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 2: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-2" class="form-control" readonly="true"  value="<?=$mail_2?>"/>
                                </div>                                 
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 3: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-3" class="form-control" readonly="true"  value="<?=$mail_3?>"/>
                                </div>                                 

                                <div class="col-md-1">
                                    <label class="control-label">E-mail 4: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-4" class="form-control" readonly="true"  value="<?=$mail_4?>"/>
                                </div>                                 
                            </div>                             

                         
                            </div>                        

                           
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="mainDiv">

            <h4></h4>

            <p style="padding-top: 20px"></p></div>
        <div class="btns">

        </div>
    </div>
</body>
</html>
