<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title></title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshowEng.js" type="text/javascript"></script>
        <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/FetchData.js" type="text/javascript"></script>
        <link rel="stylesheet" href="css/messi.min.css" />
        <script src="js/messi.js"></script>
	<script type="text/javascript">
            
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});



      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.

    <?php include('mysqlFetch.php') ?>
       
      function drawChart() {        

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Average');
         data.addRows([
          ['Language skills (english, german, french)', <?php echo $s9; ?>],
          ['The EVS country language', <?php echo $s10; ?>],
          ['Motivation to learn foreign languages', <?php echo $s11; ?>],
          ['Motivation to gain international qualifications', <?php echo $s12; ?>],
          ['Photographic skills', <?php echo $s13; ?>],
          ['Organizing your work time, time during the project', <?php echo $s14; ?>],
          ['Presentation skills in foreign language', <?php echo $s15; ?>],
          ['Being able to take care of disabled people', <?php echo $s16; ?>],
          ['Being able to take care of children and the youth', <?php echo $s17; ?>],
          ['Ability to promote events, trainings', <?php echo $s18; ?>],
          ['Ability to create initiatives / projects', <?php echo $s19; ?>],
          ['Ability to work in a project team', <?php echo $s20; ?>],
          ['Work skills related to nature preservation', <?php echo $s21; ?>],
          ['Work skills related to healthy way of life', <?php echo $s22; ?>],
          ['Teaching skills of sports and recreation', <?php echo $s23; ?>],
          ['Theatre - acting skills', <?php echo $s24; ?>],
          ['Journalist skills', <?php echo $s25; ?>],
          ['Management of computer and websites', <?php echo $s26; ?>],
          ['Other', <?php echo $s27; ?>]
          ]);
        
        // Set chart options
        var options = {title:'What work abilities did you improve during the European Voluntary?',
                     height:700,
                     backgroundColor: '#F8F8F8',
                     chartArea: {left: '300',top: '100',width: '50%'},
                     hAxis: {minValue: 0, maxValue: 6, gridlines: {color: '#DCDCDC', count: 7}}
           };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      
       function drawChart2(c) {        

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Average');
         data.addRows([
          ['Language skills (english, german, french)',c[0] ],
          ['The EVS country language',c[1] ],
          ['Motivation to learn foreign languages',c[2] ],
          ['Motivation to gain international qualifications',c[3] ],
          ['Photographic skills', c[4]],
          ['Organizing your work time, time during the project',c[5] ],
          ['Presentation skills in foreign language',c[6] ],
          ['Being able to take care of disabled people',c[7] ],
          ['Being able to take care of children and the youth',c[8] ],
          ['Ability to promote events, trainings', c[9]],
          ['Ability to create initiatives / projects',c[10] ],
          ['Ability to work in a project team', c[11]],
          ['Work skills related to nature preservation', c[12]],
          ['Work skills related to healthy way of life',c[13] ],
          ['Teaching skills of sports and recreation', c[14]],
          ['Theatre - acting skills',c[15]],
          ['Journalist skills', c[16]],
          ['Management of computer and websites', c[17]],
          ['Other', c[18]]
          ]);
        
        // Set chart options
        var options = {title:'What work abilities did you improve during the European Voluntary?',
                     height:700,
                     backgroundColor: '#F8F8F8',
                     chartArea: {left: '300',top: '100',width: '50%'},
                     hAxis: {minValue: 0, maxValue: 6, gridlines: {color: '#DCDCDC', count: 7}}
           };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
    
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="indexAdminEng.html">Admin panel</a></h1>
			
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Administrator</p>

			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
		<div class="languageButton"><img src="images/flags/Polski.png" alt="some_text" onclick="location.href='adminPersonal.php'"></div>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
				<hr/>
		<h3>Statistics</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminPersonalEng.php">Personal skills</a></li>
                        <li class="icn_categories"><a href="adminProffesionalEng.php">Proffesional skills</a></li>
			<li class="icn_categories"><a href="adminVoluntaryEng.php">Volunteers</a></li>

		</ul>
		<h3>Surveys</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminSurveyViewEng.php">Survey list</a></li>
		</ul>

		
		<footer>
			
	</aside><!-- end of sidebar -->
	
<section id="main" class="column">
            <div class="FilterDiv">
            <h4 ><input type="checkbox" name="data[]"  id="data" >Time of stay: </h4>
            <div class="FilterOptions">

            <tr>   <b>Since:</b>
                
                <td  align=left  >

                Month
                <select id="fmonth" name="fmonth">Choose Month</option>
                <option value='01'>January</option>
                <option value='02'>February</option>
                <option value='03'>March</option>
                <option value='04'>April</option>
                <option value='05'>May</option>
                <option value='06'>June</option>
                <option value='07'>July</option>
                <option value='08'>August</option>
                <option value='09'>September</option>
                <option value='10'>October</option>
                <option value='11'>November</option>
                <option value='12'>December</option>
                </select>

                </td>
                <td  align=left  >
                Year <input id="fyear" type=text name=fyear size=4 value=2005>
               
                <td  align=left  ><b>To:</b>

                <td  align=left  >

                Month
                <select id="tmonth" name="tmonth">Choose Month</option>
                <option value='01'>January</option>
                <option value='02'>February</option>
                <option value='03'>March</option>
                <option value='04'>April</option>
                <option value='05'>May</option>
                <option value='06'>June</option>
                <option value='07'>July</option>
                <option value='08'>August</option>
                <option value='09'>September</option>
                <option value='10'>October</option>
                <option value='11'>November</option>
                <option value='12'>December</option>
                </select>

                </td>
                <td  align=left  >
                Year <input id="tyear" type=text name=tyear size=4 value=2005>

            </div>
            <h4><input type="checkbox" name="kraj[]"  id="kraj" >Country:</h4>
            <div class="FilterOptions">

            <select  id="country"  name="country">
          <?php


$nationalityList1 = array("Afganistan", "Albania", "Algieria", "Andora", "Angola", "Anguilla", "Antarktyda", 
    "Antigua i Barbuda", "Arabia Saudyjska", "Argentyna", "Armenia", "Aruba", "Australia", 
    "Austria", "Azerbejdżan", "Bahamy", "Bahrajn", "Bangladesz", "Barbados", "Belgia", "Belize", 
    "Benin", "Bermudy", "Bhutan", "Białoruś", "Boliwia", "Bonaire, Sint Eustatius i Saba", 
    "Bośnia i Hercegowina", "Botswana", "Brazylia", "Brunei Darussalam", "Bułgaria", 
    "Burkina Faso", "Burundi", "Ceuta", "Chile", "Chiny", "Curaçao", "Chorwacja", "Cypr", 
    "Czad", "Czarnogóra", "Dania", "Dominika", "Dominikana", "Dżibuti", "Egipt", "Ekwador", 
    "Erytrea", "Estonia", "Etiopia", "Falklandy", "Fidżi Republika", "Filipiny", "Finlandia", 
    "Francja", "Gabon", "Gambia", "Ghana", "Gibraltar", "Grecja", "Grenada", "Grenlandia", 
    "Gruzja", "Guam", "Gujana", "Gwatemala", "Gwinea", "Gwinea Równikowa", "Gwinea-Bissau", 
    "Haiti", "Hiszpania", "Honduras", "Hongkong", "Indie", "Indonezja", "Irak", "Iran", 
    "Irlandia", "Islandia", "Izrael", "Jamajka", "Japonia", "Jemen", "Jordania", "Kajmany", 
    "Kambodża", "Kamerun", "Kanada", "Katar", "Kazachstan", "Kenia", "Kirgistan", "Kiribati", 
    "Kolumbia", "Komory", "Kongo", "Kongo, Republika Demokratyczna", 
    "Koreańska Republika Ludowo-Demokratyczna", "Kosowo", "Kostaryka", "Kuba", "Kuwejt", 
    "Laos", "Lesotho", "Liban", "Liberia", "Libia", "Liechtenstein", "Litwa", "Luksemburg", 
    "Łotwa", "Macedonia", "Madagaskar", "Majotta", "Makau", "Malawi", "Malediwy", "Malezja", 
    "Mali", "Malta", "Mariany Północne", "Maroko", "Mauretania", "Mauritius", "Meksyk", 
    "Melilla", "Mikronezja", "Mołdowa", "Mongolia", "Montserrat", "Mozambik", "Myanmar (Burma)", 
    "Namibia", "Nauru", "Nepal", "Niderlandy", "Niemcy", "Niger", "Nigeria", "Nikaragua", 
    "Niue", "Norfolk", "Norwegia", "Nowa Kaledonia", 
    "Nowa Zelandia", "Okupowane Terytorium Palestyny", "Oman", "Pakistan", "Palau", "Panama", 
    "Papua Nowa Gwinea", "Paragwaj", "Peru", "Pitcairn", "Polinezja Francuska", "Polska", 
    "Południowa Georgia i Południowe Wyspy Sandwich", "Portugalia", "Republika Czeska", 
    "Republika Korei", "Rep.Połud.Afryki", "Rep.Środkowoafryańska", "Rosja", "Rwanda", 
    "Sahara Zachodnia", "Saint Barthelemy", "Rumunia", "Salwador", "Samoa", "Samoa Amerykańskie", 
    "San Marino", "Senegal", "Serbia", "Seszele", "Sierra Leone", "Singapur", "Suazi", "Słowacja", 
    "Słowenia", "Somalia", "Sri Lanka", "St. Pierre i Miquelon", "St.Kitts i Nevis", "St.Lucia", 
    "St.Vincent i Grenadyny", "Stany Zjedn. Ameryki", "Sudan", "Sudan Południowy", "Surinam", 
    "Syria", "Szwajcaria", "Szwecja", "Święta Helena", "Tadżykistan", "Tajlandia", "Tajwan", 
    "Tanzania", "Togo", "Tokelau", "Tonga", "Trynidad i Tobago", "Tunezja", "Turcja", "Turkmenistan", 
    "Wyspy Turks i Caicos", "Tuvalu", "Uganda", "Ukraina", "Urugwaj", "Uzbekistan", "Vanuatu", "Wallis i Futuna", 
    "Watykan", "Wenezuela", "Węgry", "Wielka Brytania", "Wietnam", "Włochy", "Wschodni Timor", "Wyb.Kości Słoniowej", 
    "Wyspa Bouveta", "Wyspa Bożego Narodzenia", "Wyspy Cooka", "Wyspy Owcze", "Wyspy Marshalla", "Wyspy Salomona", 
    "Wyspy Św.Tomasza i Książęca", "Zambia", "Zielony Przylądek", "Zimbabwe", "Zjedn.Emiraty Arabskie");

$nationalityList = array( "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", 
    "Antarctica", "Antigua and Barbuda", "Saudi Arabia", "Argentina", 
    "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", 
    "Bahrain", "Bangladesh", "Barbados", "Belgium", "Belize", "Benin", 
    "Bermuda", "Bhutan", "Belarus", "Bolivia", "Bonaire, Sint Eustatius and Saba", 
    "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", 
    "Bulgaria", "Burkina Faso", "Burundi", "Ceuta", "Chile", "China", "Curaçao", 
    "Croatia", "Cyprus", "Chad", "Montenegro", "Denmark", "Dominica", 
    "Dominican Republic", "Djibouti", "Egypt", "Ecuador", "Eritrea", "Estonia", 
    "Ethiopia", "Falkland Islands (Malvinas)", "Fiji", "Philippines", "Finland", 
    "France", "Gabon", "Gambia", "Ghana", "Gibraltar", "Greece", "Grenada", 
    "Greenland", "Georgia", "Guam", "Guyana", "Guatemala", "Guinea", "Equatorial Guinea", 
    "Guinea-Bissau", "Haiti", "Spain", "Honduras", "Hong Kong", "India", "Indonesia", 
    "Iraq", "Iran, Islamic Republic of", "Ireland", "Iceland", "Israel", "Jamaica", 
    "Japan", "Yemen", "Jordan", "Cayman Islands", "Cambodia", "Cameroon", "Canada", 
    "Qatar", "Kazakhstan", "Kenya", "Kyrgyztan", "Kiribati", "Colombia", "Comoros", 
    "Congo (Rep.)", "Congo (Dem. Rep.)", "Korea, Democratic People’s Republic of", 
    "Kosovo", "Costa Rica", "Cuba", "Kuwait", "Lao, People’s Democratic Republic of", 
    "Lesotho", "Lebanon", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", 
    "Luxembourg", "Latvia", "Macedonia, the former Yugoslav Republic of", "Madagascar", 
    "Mayotte", "Macao", "Malawi", "Maldives", "Malaysia", "Mali", "Malta", "Northern Mariana Islands", 
    "Morocco", "Mauritania", "Mauritius", "Mexico", "Melilla", 
    "Micronesia, Federal States of", "Moldova, Republic of", "Mongolia", "Montserrat", 
    "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Germany", 
    "Niger", "Nigeria", "Nicaragua", "Niue", "Norfolk Island", "Norway", "New Caledonia", 
    "New Zealand", "Palestinian Territory, Occupied", "Oman", "Pakistan", "Palau", "Panama", 
    "Papua New Guinea", "Paraguay", "Peru", "Pitcairn", "French Polynesia", "Poland", 
    "South Georgia and South Sandwich Islands", "Portugal", "Czech Republic", 
    "Korea, Republic of", "South Africa", "Central African Republic", "Russian Federation", 
    "Rwanda", "Western Sahara", "Saint Barthelemy", "Romania", "EI Salvador", "Samoa", 
    "American Samoa", "San Marino", "Senegal", "Serbia", "Seychelles", "Sierra Leone", 
    "Singapore", "Swaziland", "Slovakia", "Slovenia", "Somalia", "Sri Lanka", "St Pierre and Miquelon", 
    "St.Kittsand and Nevis", "St.Lucia", "St Vincent and the Grenadines", "United States of America", 
    "Sudan", "South Sudan", "Suriname", "Syrian Arab Republic", "Switzerland", "Sweden", "St.Helena", 
    "Tajikistan", "Thailand", "Taiwan, Provice of China", "Tanzania, Unitek Republic of", "Togo", 
    "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", 
    "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "Uruguay", "Uzbekistan", 
    "Vanuatu", "WalIis and Futuna Islands", "Vatican City State", "Venezuela", "Hungary", 
    "United Kingdom", "Viet - Nam", "Italy", "Timor Leste", "Cote D’Ivoire", "Bouvet Island", 
    "Christmas Islands", "Cook Islands", "Faroe Islands", "Marshall Islands ", "Solomon Islands", 
    "Sao Tome and Principe", "Zambia", "Cape Verde", "Zimbabwe", "United Arab Emirates");
    
    foreach ($nationalityList1 as $index => $x ) {
echo '<option value="' . $x  . '">' . $nationalityList[$index] . '<option />';
}
?>
            </select>
            </div>
            <br/>
            <h4><input type="checkbox" name="projekt[]"  id="projekt" >Project type:</h4>
            <div class="FilterOptions">

            <label class="checkBox"><input type="checkbox" class="radio" value="1" name="fooby[]" id="cbType"/>Social - Integration</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="2" name="fooby[]" id="cbType"/>Helping disabled people</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="3" name="fooby[]" id="cbType"/>Working with young people</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="4" name="fooby[]" id="cbType"/>Working with children</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="5" name="fooby[]" id="cbType"/>Cultural - Artistic</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="6" name="fooby[]" id="cbType"/>Media and promotion</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="7" name="fooby[]" id="cbType"/>Enterprise, development of competition</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="8" name="fooby[]" id="cbType"/>Environmental and Ecological</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="9" name="fooby[]" id="cbType"/>Related to tolerance</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="10" name="fooby[]" id="cbType"/>Tourist - Recreation</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="11" name="fooby[]" id="cbType"/>Sport - Recreation</label>

            <script>
                var checkBoxCount=0;

                $("input:checkbox").click(function() {
                
               if($(this).is("#cbType")){
                            
                if ($(this).is(":checked")) {

                    if(checkBoxCount<3){
                    checkBoxCount++;
                    $(this).prop("checked", true);

                } else {
                        new Messi('You can check only 3 project types', {title: 'Message', modal: true});
                $(this).prop("checked", false);

                }} 

                else {
                    $(this).prop("checked", false);
                    checkBoxCount--;
                }
            }
                });
            </script>
            </div>
            <br/>
            <button id="button2"  onClick="showUser2(document.getElementById('fmonth').value,document.getElementById('tmonth').value,document.getElementById('fyear').value,document.getElementById('tyear').value,checkBoxCount);">Filter</button>
            </div>
             <div id="chart_div" style="margin-left: 10px;"></div>

	</section>


</body>

</html>

