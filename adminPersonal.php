<!doctype html>
<html lang="pl">

<head>
	<meta charset="utf-8"
	<title></title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
        <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/FetchData.js" type="text/javascript"></script>
        <link rel="stylesheet" href="css/messi.min.css" />
        <script src="js/messi.js"></script>
	<script type="text/javascript">
	

  
    $(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
        
   	 } 
	);
              
	$(document).ready(function() {
 	

	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
  
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});



      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
   google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.

        
      function drawChart() {        
  <?php include('mysqlFetch.php') ?>
      
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Średnia ocen');
        data.addRows([
          ['Umiejętność dostrzegania potrzeb innych ludzi', <?php echo $s1; ?>],
          ['Wrażliwość (świadomość emocjonalna) odmiennej kultury', <?php echo $s2; ?>],
          ['Otwartość na ludzi, nowe znajomości i przyjaźnie', <?php echo $s3; ?>],
          ['Samodzielność w czynnościach dnia codziennego', <?php echo $s4; ?>],
          ['Wrażliwość na potrzeby osób niepełnosprawnych', <?php echo $s5; ?>],
          ['Samodzielność w podejmowaniu decyzji', <?php echo $s6; ?>],
          ['Pewność siebie', <?php echo $s7; ?>],
          ['Asertywność', <?php echo $s8; ?>]
          ]);

        // Set chart options
        var options = { title:'Jakie kompetencje osobiste podniósł/a Pan/ Pani podczas Wolontariatu Europejskiego?',
                        backgroundColor: '#F8F8F8',
                        height:500,
                        chartArea: {left: '300',top: '100',width: '50%'},
                        hAxis: {minValue: 0, maxValue: 6, gridlines: {color: '#DCDCDC', count: 7}
            }

           };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      
                  function drawChart2(c) {        
  
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Średnia ocen');
        data.addRows([
          ['Umiejętność dostrzegania potrzeb innych ludzi', c[0]],
          ['Wrażliwość (świadomość emocjonalna) odmiennej kultury',  c[1]],
          ['Otwartość na ludzi, nowe znajomości i przyjaźnie', c[2]],
          ['Samodzielność w czynnościach dnia codziennego',  c[3]],
          ['Wrażliwość na potrzeby osób niepełnosprawnych',  c[4]],
          ['Samodzielność w podejmowaniu decyzji',  c[5]],
          ['Pewność siebie',  c[6]],
          ['Asertywność',  c[7]]
          ]);

        // Set chart options
        var options = { title:'Jakie kompetencje osobiste podniósł/a Pan/ Pani podczas Wolontariatu Europejskiego?',
                        backgroundColor: '#F8F8F8',
                        height:500,
                        chartArea: {left: '300',top: '100',width: '50%'},
                        hAxis: {minValue: 0, maxValue: 6, gridlines: {color: '#DCDCDC', count: 7}
            }

           };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      
    
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
 
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="indexAdmin.html">Panel administracyjny</a></h1>
			
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Administrator</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
		<div class="languageButton"><img src="images/flags/Angielski.png" alt="some_text" onclick="location.href='adminPersonalEng.php'"></div>
		
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<hr/>
		<h3>Statystyki</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminPersonal.php">Kompetencjie personalne</a></li>
                        <li class="icn_categories"><a href="adminProffesional.php">Kompetencjie zawodowe</a></li>
			<li class="icn_categories"><a href="adminVoluntary.php">Wolontariusze</a></li>

		</ul>
		<h3>Ankiety</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminSurveyView.php">Podgląd ankiet</a></li>
		</ul>

		
		<footer>
			
	</aside><!-- end of sidebar -->
       
	<section id="main" class="column">
             <div class="FilterDiv">
            <h4 ><input type="checkbox" name="data[]"  id="data" >Okres pobytu: </h4>
            <div class="FilterOptions">
            
                <tr><b>Od:</b>
                
                <td align=left>

                Miesiąc
                <select id="fmonth" name="fmonth" >Wybierz Miesiąc</option>
                <option value='01'>Styczeń</Stycze></option>
                <option value='02'>Luty</option>
                <option value='03'>Marzec</option>
                <option value='04'>Kwiecień</Kwiecie></option>
                <option value='05'>Maj</option>
                <option value='06'>Czerwiec</option>
                <option value='07'>Lipiec</option>
                <option value='08'>Sierpień</option>
                <option value='09'>Wrzesie</option>
                <option value='10'>Październik</option>
                <option value='11'>Listopad</option>
                <option value='12'>Grudzień</option>
                </select>

                </td>
                <td  align=left  >
                Rok <input id="fyear" type=text name=fyear size=4 value='2005'>

                <td  align=left><b>Do:</b>
                <td  align=left  >

                Miesiąc
                <select  id="tmonth" name="tmonth" >Wybierz Miesiąc</option>
                <option value='01'>Styczeń</Stycze></option>
                <option value='02'>Luty</option>
                <option value='03'>Marzec</option>
                <option value='04'>Kwiecień</Kwiecie></option>
                <option value='05'>Maj</option>
                <option value='06'>Czerwiec</option>
                <option value='07'>Lipiec</option>
                <option value='08'>Sierpień</option>
                <option value='09'>Wrzesie</option>
                <option value='10'>Październik</option>
                <option value='11'>Listopad</option>
                <option value='12'>Grudzień</option>
                </select>

                </td>
                <td  align=left  >
                Rok <input id="tyear" type=text name=tyear size=4 value=2005>
                
                </div>
                <h4><input type="checkbox" name="kraj[]"  id="kraj" >Panstwo:</h4>
                <div class="FilterOptions">
                <select  id="country"  name="country">
                
                    <?php

                $nationalityList = array("Afganistan", "Albania", "Algieria", "Andora", "Angola", "Anguilla", "Antarktyda", 
                    "Antigua i Barbuda", "Arabia Saudyjska", "Argentyna", "Armenia", "Aruba", "Australia", 
                    "Austria", "Azerbejdżan", "Bahamy", "Bahrajn", "Bangladesz", "Barbados", "Belgia", "Belize", 
                    "Benin", "Bermudy", "Bhutan", "Białoruś", "Boliwia", "Bonaire, Sint Eustatius i Saba", 
                    "Bośnia i Hercegowina", "Botswana", "Brazylia", "Brunei Darussalam", "Bułgaria", 
                    "Burkina Faso", "Burundi", "Ceuta", "Chile", "Chiny", "Curaçao", "Chorwacja", "Cypr", 
                    "Czad", "Czarnogóra", "Dania", "Dominika", "Dominikana", "Dżibuti", "Egipt", "Ekwador", 
                    "Erytrea", "Estonia", "Etiopia", "Falklandy", "Fidżi Republika", "Filipiny", "Finlandia", 
                    "Francja", "Gabon", "Gambia", "Ghana", "Gibraltar", "Grecja", "Grenada", "Grenlandia", 
                    "Gruzja", "Guam", "Gujana", "Gwatemala", "Gwinea", "Gwinea Równikowa", "Gwinea-Bissau", 
                    "Haiti", "Hiszpania", "Honduras", "Hongkong", "Indie", "Indonezja", "Irak", "Iran", 
                    "Irlandia", "Islandia", "Izrael", "Jamajka", "Japonia", "Jemen", "Jordania", "Kajmany", 
                    "Kambodża", "Kamerun", "Kanada", "Katar", "Kazachstan", "Kenia", "Kirgistan", "Kiribati", 
                    "Kolumbia", "Komory", "Kongo", "Kongo, Republika Demokratyczna", 
                    "Koreańska Republika Ludowo-Demokratyczna", "Kosowo", "Kostaryka", "Kuba", "Kuwejt", 
                    "Laos", "Lesotho", "Liban", "Liberia", "Libia", "Liechtenstein", "Litwa", "Luksemburg", 
                    "Łotwa", "Macedonia", "Madagaskar", "Majotta", "Makau", "Malawi", "Malediwy", "Malezja", 
                    "Mali", "Malta", "Mariany Północne", "Maroko", "Mauretania", "Mauritius", "Meksyk", 
                    "Melilla", "Mikronezja", "Mołdowa", "Mongolia", "Montserrat", "Mozambik", "Myanmar (Burma)", 
                    "Namibia", "Nauru", "Nepal", "Niderlandy", "Niemcy", "Niger", "Nigeria", "Nikaragua", 
                    "Niue", "Norfolk", "Norwegia", "Nowa Kaledonia", 
                    "Nowa Zelandia", "Okupowane Terytorium Palestyny", "Oman", "Pakistan", "Palau", "Panama", 
                    "Papua Nowa Gwinea", "Paragwaj", "Peru", "Pitcairn", "Polinezja Francuska", "Polska", 
                    "Południowa Georgia i Południowe Wyspy Sandwich", "Portugalia", "Republika Czeska", 
                    "Republika Korei", "Rep.Połud.Afryki", "Rep.Środkowoafryańska", "Rosja", "Rwanda", 
                    "Sahara Zachodnia", "Saint Barthelemy", "Rumunia", "Salwador", "Samoa", "Samoa Amerykańskie", 
                    "San Marino", "Senegal", "Serbia", "Seszele", "Sierra Leone", "Singapur", "Suazi", "Słowacja", 
                    "Słowenia", "Somalia", "Sri Lanka", "St. Pierre i Miquelon", "St.Kitts i Nevis", "St.Lucia", 
                    "St.Vincent i Grenadyny", "Stany Zjedn. Ameryki", "Sudan", "Sudan Południowy", "Surinam", 
                    "Syria", "Szwajcaria", "Szwecja", "Święta Helena", "Tadżykistan", "Tajlandia", "Tajwan", 
                    "Tanzania", "Togo", "Tokelau", "Tonga", "Trynidad i Tobago", "Tunezja", "Turcja", "Turkmenistan", 
                    "Wyspy Turks i Caicos", "Tuvalu", "Uganda", "Ukraina", "Urugwaj", "Uzbekistan", "Vanuatu", "Wallis i Futuna", 
                    "Watykan", "Wenezuela", "Węgry", "Wielka Brytania", "Wietnam", "Włochy", "Wschodni Timor", "Wyb.Kości Słoniowej", 
                    "Wyspa Bouveta", "Wyspa Bożego Narodzenia", "Wyspy Cooka", "Wyspy Owcze", "Wyspy Marshalla", "Wyspy Salomona", 
                    "Wyspy Św.Tomasza i Książęca", "Zambia", "Zielony Przylądek", "Zimbabwe", "Zjedn.Emiraty Arabskie");
                foreach ($nationalityList as $cataloguePDFName) {
                echo '<option value="' . $cataloguePDFName . '">' . $cataloguePDFName . '<option />';
                }
                ?>
            </select>
                </div>
                <br/>
            <h4><input type="checkbox" name="projekt[]"  id="projekt" >Typ projektu:</h4>
            <div class="FilterOptions">

            <label class="checkBox"><input type="checkbox" class="radio" value="1" name="fooby[]" id="cbType"/>Społeczno-integracyjne</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="2" name="fooby[]" id="cbType"/>Pomocy osobom niepełnosprawnym</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="3" name="fooby[]" id="cbType"/>Współpracy z młodzieżą</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="4" name="fooby[]" id="cbType"/>Współpracy z dziećmi</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="5" name="fooby[]" id="cbType"/>Kulturalno-artystyczne</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="6" name="fooby[]" id="cbType"/>Medialne i promocyjne</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="7" name="fooby[]" id="cbType"/>Przedsiębiorcze, rozwoju konkurencji</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="8" name="fooby[]" id="cbType"/>Środowiskowe i ekologiczne</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="9" name="fooby[]" id="cbType"/>Związane z tolerancją</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="10" name="fooby[]" id="cbType"/>Turystyczno-rekreacyjne</label>
            <label class="checkBox"><input type="checkbox" class="radio" value="11" name="fooby[]" id="cbType"/>Sportowo-rekreacyjne lub taneczne</label>

            <script>
                var checkBoxCount=0;
                 var table=[0,0,0];
                $("input:checkbox").click(function() {
                
               if($(this).is("#cbType")){
                            
                if ($(this).is(":checked")) {

                    if(checkBoxCount<3){
                 
                    checkBoxCount++;
                    $(this).prop("checked", true);

                } else {
                        new Messi('Maksymalnie można zaznaczyć 3 typy projektów', {title: 'Powiadomienie', modal: true});
                $(this).prop("checked", false);

                }} 

                else {
                    $(this).prop("checked", false);
                    checkBoxCount--;
                   
                }
            }
                });
            </script>
            </div>
            <br/>
            <button id="button2"  onClick="showUser(document.getElementById('fmonth').value,document.getElementById('tmonth').value,document.getElementById('fyear').value,document.getElementById('tyear').value,checkBoxCount);">Filtruj</button>
            </div>
             <div id="chart_div" style="margin-left: 10px;"></div>
        

		
	</section>


 
</body>

</html>

