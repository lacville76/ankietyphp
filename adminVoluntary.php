<!doctype html>
<html lang="pl">

<head>
	<meta charset="utf-8"/>
	<title></title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	 <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
         <script type="text/javascript" src="js/FetchDataCountries.js" ></script>
         <script type="text/javascript">
            
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
        
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
                
		return false;
	});

});

  google.load('visualization', '1.0', {'packages':['corechart']});

  
   


function drawRegionsMap(c) {
   
   var nations= ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", 
    "Antarctica", "Antigua and Barbuda", "Saudi Arabia", "Argentina", 
    "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", 
    "Bahrain", "Bangladesh", "Barbados", "Belgium", "Belize", "Benin", 
    "Bermuda", "Bhutan", "Belarus", "Bolivia", "Bonaire, Sint Eustatius and Saba", 
    "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", 
    "Bulgaria", "Burkina Faso", "Burundi", "Ceuta", "Chile", "China", "Curaçao", 
    "Croatia", "Cyprus", "Chad", "Montenegro", "Denmark", "Dominica", 
    "Dominican Republic", "Djibouti", "Egypt", "Ecuador", "Eritrea", "Estonia", 
    "Ethiopia", "Falkland Islands (Malvinas)", "Fiji", "Philippines", "Finland", 
    "France", "Gabon", "Gambia", "Ghana", "Gibraltar", "Greece", "Grenada", 
    "Greenland", "Georgia", "Guam", "Guyana", "Guatemala", "Guinea", "Equatorial Guinea", 
    "Guinea-Bissau", "Haiti", "Spain", "Honduras", "Hong Kong", "India", "Indonesia", 
    "Iraq", "Iran", "Ireland", "Iceland", "Israel", "Jamaica", 
    "Japan", "Yemen", "Jordan", "Cayman Islands", "Cambodia", "Cameroon", "Canada", 
    "Qatar", "Kazakhstan", "Kenya", "Kyrgyztan", "Kiribati", "Colombia", "Comoros", 
    "CG", "CD", "KP",  
    "Kosovo", "Costa Rica", "Cuba", "Kuwait", "Lao, People’s Democratic Republic of", 
    "Lesotho", "Lebanon", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", 
    "Luxembourg", "Latvia", "Macedonia, the former Yugoslav Republic of", "Madagascar", 
    "Mayotte", "Macao", "Malawi", "Maldives", "Malaysia", "Mali", "Malta", "Northern Mariana Islands", 
    "Morocco", "Mauritania", "Mauritius", "Mexico", "Melilla", 
    "Micronesia, Federal States of", "Moldova, Republic of", "Mongolia", "Montserrat", 
    "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Germany", 
    "Niger", "Nigeria", "Nicaragua", "Niue", "Norfolk Island", "Norway", "New Caledonia", 
    "New Zealand", "Palestinian Territory, Occupied", "Oman", "Pakistan", "Palau", "Panama", 
    "Papua New Guinea", "Paraguay", "Peru", "Pitcairn", "French Polynesia", "Poland", 
    "South Georgia and South Sandwich Islands", "Portugal", "Czech Republic", 
    "Korea South", "South Africa", "Central African Republic", "Russia", 
    
    "Rwanda", "Western Sahara", "Saint Barthelemy", "Romania", "EI Salvador", "Samoa", 
    "American Samoa", "San Marino", "Senegal", "Serbia", "Seychelles", "Sierra Leone", 
    "Singapore", "Swaziland", "Slovakia", "Slovenia", "Somalia", "Sri Lanka", "St Pierre and Miquelon", 
    "St.Kittsand and Nevis", "St.Lucia", "St Vincent and the Grenadines", "United States", 
    "Sudan", "South Sudan", "Suriname", "Syrian Arab Republic", "Switzerland", "Sweden", "St.Helena", 
    "Tajikistan", "Thailand", "Taiwan, Provice of China", "Tanzania", "Togo", 
    "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", 
    "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "Uruguay", "Uzbekistan", 
    "Vanuatu", "WalIis and Futuna Islands", "Vatican City State", "Venezuela", "Hungary", 
    "United Kingdom", "VietNam", "Italy", "Timor Leste", "Cote D’Ivoire", "Bouvet Island", 
    "Christmas Islands", "Cook Islands", "Faroe Islands", "Marshall Islands ", "Solomon Islands", 
    "Sao Tome and Principe", "Zambia", "Cape Verde", "Zimbabwe", "United Arab Emirates"];
     var data = new google.visualization.DataTable();
        data.addRows(250);
      data.addColumn('string', 'Country');
      data.addColumn('number', 'Popularity');
      for(var i=0;i<250;i++)
          {
              if(typeof c[i] == 'undefined') c[i]=0;
              console.log(c[i]);
              data.setValue(i, 0, nations[i]);
      data.setValue(i, 1, c[i]);
   
          }
    
      var geochart = new google.visualization.GeoChart(
          document.getElementById('chart_div'));
      geochart.draw(data, {width: 556, height: 347, backgroundColor: '#F8F8F8'});
      
};
 

 
     
 showUser3(0,0,0,0);
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
        
    });
    
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="indexAdmin.html">Panel administracyjny</a></h1>
			
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Administrator</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
		<div class="languageButton"><img src="images/flags/Angielski.png" alt="some_text" onclick="location.href='adminVoluntaryEng.php'"></div>
		
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<hr/>
		<h3>Statystyki</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminPersonal.php">Kompetencjie personalne</a></li>
                        <li class="icn_categories"><a href="adminProffesional.php">Kompetencjie zawodowe</a></li>
			<li class="icn_categories"><a href="adminVoluntary.php">Wolontariusze</a></li>

		</ul>
		<h3>Ankiety</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="adminSurveyView.php">Podgląd ankiet</a></li>
		</ul>

		
		<footer>
			
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
                <div class="FilterDiv">
            <h4 ><input type="checkbox" name="data[]"  id="data" >Okres pobytu: </h4>
            <div class="FilterOptions">
            
                <tr><b>Od:</b>
                
                <td align=left>

                Miesiąc
                <select id="fmonth" name="fmonth" >Wybierz Miesiąc</option>
                <option value='01'>Styczeń</Stycze></option>
                <option value='02'>Luty</option>
                <option value='03'>Marzec</option>
                <option value='04'>Kwiecień</Kwiecie></option>
                <option value='05'>Maj</option>
                <option value='06'>Czerwiec</option>
                <option value='07'>Lipiec</option>
                <option value='08'>Sierpień</option>
                <option value='09'>Wrzesie</option>
                <option value='10'>Październik</option>
                <option value='11'>Listopad</option>
                <option value='12'>Grudzień</option>
                </select>

                </td>
                <td  align=left  >
                Rok <input id="fyear" type=text name=fyear size=4 value='2005'>

                <td  align=left><b>Do:</b>
                <td  align=left  >

                Miesiąc
                <select  id="tmonth" name="tmonth" >Wybierz Miesiąc</option>
                <option value='01'>Styczeń</Stycze></option>
                <option value='02'>Luty</option>
                <option value='03'>Marzec</option>
                <option value='04'>Kwiecień</Kwiecie></option>
                <option value='05'>Maj</option>
                <option value='06'>Czerwiec</option>
                <option value='07'>Lipiec</option>
                <option value='08'>Sierpień</option>
                <option value='09'>Wrzesie</option>
                <option value='10'>Październik</option>
                <option value='11'>Listopad</option>
                <option value='12'>Grudzień</option>
                </select>

                </td>
                <td  align=left  >
                Rok <input id="tyear" type=text name=tyear size=4 value=2005>
                
                </div>
             <br/>
            <button id="button2"  onClick="showUser3(document.getElementById('fmonth').value,document.getElementById('tmonth').value,document.getElementById('fyear').value,document.getElementById('tyear').value);">Pokaż</button> 
         <div id="chart_div" style="margin-left: 10px;"></div>
          <?php include('VoluntaryFetch.php') ?>
            <label>Ankieta zostala wypelniona przez <?php echo $wynik; ?> wolontariuszy.</label>
		
		
	</section>


</body>

</html>

