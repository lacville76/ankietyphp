<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="js/jquery-2.1.1.min.js"></script> 
        <script src="js/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/bootstrap/dialog/js/bootstrap-dialog.min.js"></script>
        <script src="js/bootstrap/select/bootstrap-select.min.js"></script>
        <script src="js/bootstrap/slider/js/bootstrap-slider.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="js/bootstrap/dialog/css/bootstrap-dialog.min.css" />
        <link rel="stylesheet" href="js/bootstrap/select/bootstrap-select.min.css" />
        <link rel="stylesheet" href="js/bootstrap/slider/css/slider.css" />
        <link rel="stylesheet" href="css/messi.min.css" />
        <link rel="stylesheet" href="style.css" />


        <script src="js/messi.js"></script>
        <script src="ajax1.js" type="text/javascript"></script>

        <title>European Voluntary Service - form</title>
    </head>

    <body>
        <div class="languageButton">
            <a href="index.php">
                <img src="images/flags/Polski.png" alt="Angielski" id="LangImg" title="Zmiana języka na Angielski / Change language to English">
            </a>
        </div>

        <div class="containers">
            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">EVALUATION OF THE PARTICIPATION IN THE PROJECT</div>
                    <div class="panel-body">
                        <h3 class="text-center" >European Voluntary</h3>
                        <h4 class="text-center" >If you dont have anything to type in given field, just leave it without change</h4>                    
                                  <br />

                     

                        <form Name ="form1" Method ="post" id="form1" ACTION = "insert.php">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                    <input type="text"   name="name" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Surname:</label> 
                                    <input type="text" name="surname" class="form-control" />
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>E-Mail: </label>
                                    <input type="text" name="mail" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label>Project Title:</label>  
                                    <input type="text" name="project" class="form-control" />
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-12"><label>Project types:</label></div>
                                <div class="col-md-12 checboxy checkbox">

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio" name="fooby1[]" id="fooby1"  />
                                        Social - Integration
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby2[]" id="fooby2" />
                                        Helping disabled people
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby3[]" id="fooby3" />
                                       Working with young people
                                    </label>

                                    <label class="col-md-4">
                                        <input  type="checkbox" class="radio"  name="fooby4[]"  id="fooby4"/>
                                        Working with children
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby5[]" id="fooby5" />
                                        Cultural - Artistic
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby6[]" id="fooby6" />
                                        Media and promotion
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby7[]" id="fooby7"/>
                                        Enterprise, development of competition
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby8[]" id="fooby8"/>
                                        Environmental and Ecological
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby9[]" id="fooby9"/>
                                        Related to tolerance
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio" name="fooby10[]" id="fooby10"/>
                                        Tourist - Recreation
                                    </label>

                                    <label class="col-md-4">
                                        <input type="checkbox" class="radio"  name="fooby11[]" id="fooby11"/>
                                        Sport - Recreation
                                    </label>                        
                                </div>
                            </div> 

                            <div class="form-group row selects">
                                <div class="col-md-6">
                                    <label>Time of stay :</label> <br />
                                    <div class="col-md-3">
                                        <select name="fday" class="form-control" >
                                            <option value='01'>01</option>
                                            <option value='02'>02</option>
                                            <option value='03'>03</option>
                                            <option value='04'>04</option>
                                            <option value='05'>05</option>
                                            <option value='06'>06</option>
                                            <option value='07'>07</option>
                                            <option value='08'>08</option>
                                            <option value='09'>09</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select name="fmonth" class="form-control" >
                                        <option value='01'>January</option>
                                        <option value='02'>February</option>
                                        <option value='03'>March</option>
                                        <option value='04'>April</option>
                                        <option value='05'>May</option>
                                        <option value='06'>June</option>
                                        <option value='07'>July</option>
                                        <option value='08'>August</option>
                                        <option value='09'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="fyear" class="form-control" >
                                            <option value='2005'>2005</option>
                                            <option value='2006'>2006</option>
                                            <option value='2007'>2007</option>
                                            <option value='2008'>2008</option>
                                            <option value='2009'>2009</option>
                                            <option value='2010'>2010</option>
                                            <option value='2011'>2011</option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                            <option value='2015'>2015</option>
                                            <option value='2016'>2016</option>
                                        </select>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>To:</label> <br />
                                    <div class="col-md-3">
                                        <select name="tday" class="form-control" >
                                            <option value='01'>01</option>
                                            <option value='02'>02</option>
                                            <option value='03'>03</option>
                                            <option value='04'>04</option>
                                            <option value='05'>05</option>
                                            <option value='06'>06</option>
                                            <option value='07'>07</option>
                                            <option value='08'>08</option>
                                            <option value='09'>09</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                       <select name="fmonth" class="form-control" >
                                        <option value='01'>January</option>
                                        <option value='02'>February</option>
                                        <option value='03'>March</option>
                                        <option value='04'>April</option>
                                        <option value='05'>May</option>
                                        <option value='06'>June</option>
                                        <option value='07'>July</option>
                                        <option value='08'>August</option>
                                        <option value='09'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tyear" class="form-control" >
                                            <option value='2005'>2005</option>
                                            <option value='2006'>2006</option>
                                            <option value='2007'>2007</option>
                                            <option value='2008'>2008</option>
                                            <option value='2009'>2009</option>
                                            <option value='2010'>2010</option>
                                            <option value='2011'>2011</option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                            <option value='2015'>2015</option>
                                            <option value='2016'>2016</option>
                                        </select>    
                                    </div>                                    
                                </div>
                            </div> 

                          <div class="form-group selects row">
                                <div class="col-md-6">
                                    <label>Country:</label>
                                    <select name="country" class="form-control">
     <?php
                                      $nationalityList1 = array("Afganistan", "Albania", "Algieria", "Andora", "Angola", "Anguilla", "Antarktyda",
                                            "Antigua i Barbuda", "Arabia Saudyjska", "Argentyna", "Armenia", "Aruba", "Australia",
                                            "Austria", "Azerbejdżan", "Bahamy", "Bahrajn", "Bangladesz", "Barbados", "Belgia", "Belize",
                                            "Benin", "Bermudy", "Bhutan", "Białoruś", "Boliwia", "Bonaire, Sint Eustatius i Saba",
                                            "Bośnia i Hercegowina", "Botswana", "Brazylia", "Brunei Darussalam", "Bułgaria",
                                            "Burkina Faso", "Burundi", "Ceuta", "Chile", "Chiny", "Curaçao", "Chorwacja", "Cypr",
                                            "Czad", "Czarnogóra", "Dania", "Dominika", "Dominikana", "Dżibuti", "Egipt", "Ekwador",
                                            "Erytrea", "Estonia", "Etiopia", "Falklandy", "Fidżi Republika", "Filipiny", "Finlandia",
                                            "Francja", "Gabon", "Gambia", "Ghana", "Gibraltar", "Grecja", "Grenada", "Grenlandia",
                                            "Gruzja", "Guam", "Gujana", "Gwatemala", "Gwinea", "Gwinea Równikowa", "Gwinea-Bissau",
                                            "Haiti", "Hiszpania", "Honduras", "Hongkong", "Indie", "Indonezja", "Irak", "Iran",
                                            "Irlandia", "Islandia", "Izrael", "Jamajka", "Japonia", "Jemen", "Jordania", "Kajmany",
                                            "Kambodża", "Kamerun", "Kanada", "Katar", "Kazachstan", "Kenia", "Kirgistan", "Kiribati",
                                            "Kolumbia", "Komory", "Kongo", "Kongo, Republika Demokratyczna",
                                            "Koreańska Republika Ludowo-Demokratyczna", "Kosowo", "Kostaryka", "Kuba", "Kuwejt",
                                            "Laos", "Lesotho", "Liban", "Liberia", "Libia", "Liechtenstein", "Litwa", "Luksemburg",
                                            "Łotwa", "Macedonia", "Madagaskar", "Majotta", "Makau", "Malawi", "Malediwy", "Malezja",
                                            "Mali", "Malta", "Mariany Północne", "Maroko", "Mauretania", "Mauritius", "Meksyk",
                                            "Melilla", "Mikronezja", "Mołdowa", "Mongolia", "Montserrat", "Mozambik", "Myanmar (Burma)",
                                            "Namibia", "Nauru", "Nepal", "Niderlandy", "Niemcy", "Niger", "Nigeria", "Nikaragua",
                                            "Niue", "Norfolk", "Norwegia", "Nowa Kaledonia",
                                            "Nowa Zelandia", "Okupowane Terytorium Palestyny", "Oman", "Pakistan", "Palau", "Panama",
                                            "Papua Nowa Gwinea", "Paragwaj", "Peru", "Pitcairn", "Polinezja Francuska", "Polska",
                                            "Południowa Georgia i Południowe Wyspy Sandwich", "Portugalia", "Republika Czeska",
                                            "Republika Korei", "Rep.Połud.Afryki", "Rep.Środkowoafryańska", "Rosja", "Rwanda",
                                            "Sahara Zachodnia", "Saint Barthelemy", "Rumunia", "Salwador", "Samoa", "Samoa Amerykańskie",
                                            "San Marino", "Senegal", "Serbia", "Seszele", "Sierra Leone", "Singapur", "Suazi", "Słowacja",
                                            "Słowenia", "Somalia", "Sri Lanka", "St. Pierre i Miquelon", "St.Kitts i Nevis", "St.Lucia",
                                            "St.Vincent i Grenadyny", "Stany Zjedn. Ameryki", "Sudan", "Sudan Południowy", "Surinam",
                                            "Syria", "Szwajcaria", "Szwecja", "Święta Helena", "Tadżykistan", "Tajlandia", "Tajwan",
                                            "Tanzania", "Togo", "Tokelau", "Tonga", "Trynidad i Tobago", "Tunezja", "Turcja", "Turkmenistan",
                                            "Wyspy Turks i Caicos", "Tuvalu", "Uganda", "Ukraina", "Urugwaj", "Uzbekistan", "Vanuatu", "Wallis i Futuna",
                                            "Watykan", "Wenezuela", "Węgry", "Wielka Brytania", "Wietnam", "Włochy", "Wschodni Timor", "Wyb.Kości Słoniowej",
                                            "Wyspa Bouveta", "Wyspa Bożego Narodzenia", "Wyspy Cooka", "Wyspy Owcze", "Wyspy Marshalla", "Wyspy Salomona",
                                            "Wyspy Św.Tomasza i Książęca", "Zambia", "Zielony Przylądek", "Zimbabwe", "Zjedn.Emiraty Arabskie");
                                       $nationalityList = array("Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", 
                                                    "Antarctica", "Antigua and Barbuda", "Saudi Arabia", "Argentina", 
                                                    "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", 
                                                    "Bahrain", "Bangladesh", "Barbados", "Belgium", "Belize", "Benin", 
                                                    "Bermuda", "Bhutan", "Belarus", "Bolivia", "Bonaire, Sint Eustatius and Saba", 
                                                    "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", 
                                                    "Bulgaria", "Burkina Faso", "Burundi", "Ceuta", "Chile", "China", "Curaçao", 
                                                    "Croatia", "Cyprus", "Chad", "Montenegro", "Denmark", "Dominica", 
                                                    "Dominican Republic", "Djibouti", "Egypt", "Ecuador", "Eritrea", "Estonia", 
                                                    "Ethiopia", "Falkland Islands (Malvinas)", "Fiji", "Philippines", "Finland", 
                                                    "France", "Gabon", "Gambia", "Ghana", "Gibraltar", "Greece", "Grenada", 
                                                    "Greenland", "Georgia", "Guam", "Guyana", "Guatemala", "Guinea", "Equatorial Guinea", 
                                                    "Guinea-Bissau", "Haiti", "Spain", "Honduras", "Hong Kong", "India", "Indonesia", 
                                                    "Iraq", "Iran, Islamic Republic of", "Ireland", "Iceland", "Israel", "Jamaica", 
                                                    "Japan", "Yemen", "Jordan", "Cayman Islands", "Cambodia", "Cameroon", "Canada", 
                                                    "Qatar", "Kazakhstan", "Kenya", "Kyrgyztan", "Kiribati", "Colombia", "Comoros", 
                                                    "Congo (Rep.)", "Congo (Dem. Rep.)", "Korea, Democratic People’s Republic of", 
                                                    "Kosovo", "Costa Rica", "Cuba", "Kuwait", "Lao, People’s Democratic Republic of", 
                                                    "Lesotho", "Lebanon", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", 
                                                    "Luxembourg", "Latvia", "Macedonia, the former Yugoslav Republic of", "Madagascar", 
                                                    "Mayotte", "Macao", "Malawi", "Maldives", "Malaysia", "Mali", "Malta", "Northern Mariana Islands", 
                                                    "Morocco", "Mauritania", "Mauritius", "Mexico", "Melilla", 
                                                    "Micronesia, Federal States of", "Moldova, Republic of", "Mongolia", "Montserrat", 
                                                    "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Germany", 
                                                    "Niger", "Nigeria", "Nicaragua", "Niue", "Norfolk Island", "Norway", "New Caledonia", 
                                                    "New Zealand", "Palestinian Territory, Occupied", "Oman", "Pakistan", "Palau", "Panama", 
                                                    "Papua New Guinea", "Paraguay", "Peru", "Pitcairn", "French Polynesia", "Poland", 
                                                    "South Georgia and South Sandwich Islands", "Portugal", "Czech Republic", 
                                                    "Korea, Republic of", "South Africa", "Central African Republic", "Russian Federation", 
                                                    "Rwanda", "Western Sahara", "Saint Barthelemy", "Romania", "EI Salvador", "Samoa", 
                                                    "American Samoa", "San Marino", "Senegal", "Serbia", "Seychelles", "Sierra Leone", 
                                                    "Singapore", "Swaziland", "Slovakia", "Slovenia", "Somalia", "Sri Lanka", "St Pierre and Miquelon", 
                                                    "St.Kittsand and Nevis", "St.Lucia", "St Vincent and the Grenadines", "United States of America", 
                                                    "Sudan", "South Sudan", "Suriname", "Syrian Arab Republic", "Switzerland", "Sweden", "St.Helena", 
                                                    "Tajikistan", "Thailand", "Taiwan, Provice of China", "Tanzania, Unitek Republic of", "Togo", 
                                                    "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", 
                                                    "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "Uruguay", "Uzbekistan", 
                                                    "Vanuatu", "WalIis and Futuna Islands", "Vatican City State", "Venezuela", "Hungary", 
                                                    "United Kingdom", "Viet - Nam", "Italy", "Timor Leste", "Cote D’Ivoire", "Bouvet Island", 
                                                    "Christmas Islands", "Cook Islands", "Faroe Islands", "Marshall Islands ", "Solomon Islands", 
                                                    "Sao Tome and Principe", "Zambia", "Cape Verde", "Zimbabwe", "United Arab Emirates");
                       
                        foreach ($nationalityList1 as $index => $x ) {
echo '<option value="' . $x  . '">' . $nationalityList[$index] . '</option>';
}
                                        ?>
 </select>                                    
                                </div>
                                <div class="col-md-6">
                                    <label> City:</label>  
                                    <input type="text" name="city" class="form-control"/>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Please describe the project shortly (what was it about, what was the goal,who was involved in the realization of the project, to whom the project was directed to)</label>
                                    <textarea name="charProjektu" rows="4" cols="50" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>What personal skills did you raise during the Voluntary</label>
                                </div>
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    1. Ability to notice the needs of other people:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp1" value="0" />
                                </div>
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    2. Sensitivity (emotional awareness) to other culture:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp2" value="0" />
                                </div>                                
                            </div>

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    3. Being open to meeting new people, making new friends:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp3" value="0" />
                                </div>                                 
                            </div>         

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    4. Being able to cope in every day life:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp4" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    5. Being aware to needs of disabled people:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp5" value="0">
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    6. Independent in making own decisions:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp6" value="0" />
                                </div>                                 
                            </div>       

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    7. Self-confidence:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp7" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    8. Self-assertion::
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="okomp8" value="0" />
                                </div>                                 
                            </div>    

                            <div class="form-group row">
                                <br /><br />
                                <div class="col-md-12">
                                    <label>What information / good advice would you give to other volunteers that participate in the European Voluntary</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Preperation for the project:</label>
                                    <textarea name="przygotowanie" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Journey:</label>
                                    <textarea name="podroz"rows="4" cols="50" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Place to live, food:</label>
                                    <textarea name="zakwaterowanie" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Trips:</label>
                                    <textarea name="wycieczki"  rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Project activities and their organization:</label>
                                    <textarea name="dzialania_proj" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label >Youthpass:</label>
                                    <textarea name="youthpass"  rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Other:</label>
                                    <textarea name="inne" rows="4" cols="50" class="form-control"></textarea>                                    
                                </div>
                            </div>   

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>What work abilities did you improve during the European Voluntary :</label>
                                </div>
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    1.Language skills (english, german, french):
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp1" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    2. The EVS country language:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp2" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    3. Motivation to learn foreign languages:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp3" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    4. Motivation to gain international qualifications:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp4" value="0" />
                                </div>                                 
                            </div>     

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    5. Photographic skills:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp5" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    6. Organizing your work time, time during the project:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp6" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    7. Presentation skills in foreign language:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp7" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    8. Being able to take care of disabled people:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp8" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    9. Being able to take care of children and the youth:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp9" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    10. Ability to promote events, trainings:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp10" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    11. Ability to create initiatives / projects:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp11" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    12. Ability to work in a project team:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp12" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    13. Work skills related to nature preservation:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp13" value="0" />
                                </div>                                 
                            </div>   

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    14. Work skills related to healthy way of life:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp14" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    15. Teaching skills of sports and recreation:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp15" value="0" />
                                </div>                                 
                            </div>         

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    16. Theatre - acting skills:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp16" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    17. Journalist skills:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp17" value="0" />
                                </div>                                 
                            </div> 

                            <div class="form-group row sliders">
                                <div class="col-md-8">
                                    18. Management of computer and websites:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="pkomp18" value="0" />
                                </div>                                 
                            </div>  

                            <div class="form-group row">
                                <div class="col-md-1 text-right">
                                    <label class="control-label">Other:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" name="kompetencje_zawodowe" class="form-control">
                                </div>              
                                <div class="col-md-4 sliders">
                                    <input type="text" name="pkomp19" value="0" />
                                </div>
                            </div>       

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Who would you invite to this program:</label>
                                </div>                               
                            </div>       

                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 1: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-1" class="form-control" />
                                </div>                                 
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 2: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-2" class="form-control" />
                                </div>                                 
                            </div> 

                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label class="control-label">E-mail 3: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-3" class="form-control" />
                                </div>                                 

                                <div class="col-md-1">
                                    <label class="control-label">E-mail 4: </label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="email-4" class="form-control" />
                                </div>                                 
                            </div>                             

                            <div class="form-group">
                                <div class="input-group col-md-6">
                                    <div  class="input-group-addon">
                                        <input type="checkbox" name="zgoda[]"  id="zgoda" />
                                    </div>
                                    <input name="" value="I agree to put my opinion on EVS website" class="form-control disabled" disabled="disabled" />
                                </div>
                            </div>                        

                            <div class="form-group text-right">
                                <INPUT TYPE = "Submit" id="SubmitBtn" class="btn btn-primary" Name = "Submit1" VALUE = "Send" onClick="Update();">
                                <INPUT TYPE = "Button" id="PrintBtn"  class="btn btn-default" Name = "Submit2" VALUE = "Print" onclick="printAnk();">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="mainDiv">

            <h4></h4>

            <p style="padding-top: 20px"></p></div>
        <div class="btns">

        </div>
    </div>
</body>
</html>
